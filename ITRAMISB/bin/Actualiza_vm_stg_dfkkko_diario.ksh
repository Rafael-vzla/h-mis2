#!/usr/bin/ksh
########################################################################################
# PROGRAM:              Actualiza_vm_stg_dfkkko.ksh
# AUTHOR:               Francisco Ramos La Cruz
# DATE:                 26-05-2009
# DESCRIPTION:          PROGRAMA QUE CREA Y REFRESH DE LA VM_STAGE_DFKKKO  
# PARAMETERS INPUT:     Fecha en formato YYYYMMDD para crear el archivo log
#
########################################################################################
# descomentar para debug
set -x
 #Declaracion de variables de correo electronico
 export ORACLE_TERM=vt220
 vinterfase=ITRAMISB

 #Declaracion de variables de ambiente
 export MAIPATH=/data/MIS2ADM/$vinterfase
 export LOGPATH=$MAIPATH/log
 export BADPATH=$MAIPATH/error
 export DATPATH=$MAIPATH/output
 export DATPATH1=$MAIPATH/output
 export DATPATH2=$MAIPATH/output/Done
 export VARPATH=$MAIPATH/sql
 export CONPATH=$MAIPATH/ctl

 v1=$1
 log=$LOGPATH/Actualiza_vm_stg_dfkkko_diario_$v1.log
 
 print "CFTMIUD010.sh Inicio vm_stg_dfkkko_diario fecha: $v1" > $log

# Mensaje de inicio creacion vm
mailx -s "CFTMIUD010.sh Inicio vm_stg_dfkkko_diario fecha: $v1 " MIS_Grupo@cantv.com.ve rmcapa@cantv.com.ve Adminbasis@cantv.com.ve msamue@cantv.com.ve < $log
   Actualiza_vm_stg_dfkkko_diario.sh $v1 > $LOGPATH/Actualiza_vm_stg_dfkkko_diario_$v1.log

ORA=`grep ORA- $log | wc -l `

ERROR=`grep 'ORA-' $log | cut -c1-9 |head -1`

MENSA="INTERFASE $vinterfase fallo Ora->$ORA = $ERROR "

if [ $ORA -eq 0 ]; then
   MENSA="Proceso culminado correctamente";
   mailx -s "CFTMIUD010.sh $MENSA" framos01@cantv.com.ve rmcapa@cantv.com.ve Adminbasis@cantv.com.ve msamue@cantv.com.ve < $log
   salida=0; else
   mailx -s "CFTMIUD010.sh $MENSA" framos01@cantv.com.ve rmcapa@cantv.com.ve Adminbasis@cantv.com.ve msamue@cantv.com.ve < $log
   cat $log
   salida=8
fi
exit $salida
