dropdropdrop **********

drop *******

dropdropdrop ********

drop333666 999 0000n zz

DROP VIEW RMCABSF.VW_60021_NORES_2010;

/* Formatted on 29/07/2011 12:04:53 p.m. (QP5 v5.139.911.3011) */
CREATE OR REPLACE FORCE VIEW RMCABSF.VW_60021_NORES_2010
(
   CUENTA_CONTABLE,
   MES_CONTABILIZACION,
   MONTO_DEBE,
   MONTO_HABER,
   SALDO_CUENTA,
   SALDO_ACUMULADO
)
AS
     SELECT t1.cuenta_contable,
            t1.mes_contabilizacion,
            t1.monto_debe,
            t1.monto_haber,
            t1.saldo_cuenta,
            t1.saldo_acumulado
       FROM rmcabsf.th_20914_auxiliar_resumen t1
      WHERE t1.cuenta_contable = '2120060021'
            AND t1.mes_contabilizacion BETWEEN '201001' AND '201012'
   ORDER BY 1, 2;


GRANT SELECT ON RMCABSF.VW_60021_NORES_2010 TO FBLANCO;

GRANT SELECT ON RMCABSF.VW_60021_NORES_2010 TO FLEON;

GRANT SELECT ON RMCABSF.VW_60021_NORES_2010 TO JVILLEGAS;
